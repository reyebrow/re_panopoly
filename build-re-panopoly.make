; Drush Make file
;
; Use this file to build a full distribution including Drupal core and the
; "re_panopoly" distribution using the following command...
;
; drush --no-patch-txt make build-re-panopoly.make <target directory>

api = 2
core = 7.x

; Include the definition for how to build Drupal core directly, including
; patches.

includes[] = drupal-org-core.make

; Download the re_panopoly and recursively build all
; of its dependencies.

projects[re_panopoly][type] = profile
; The following two lines allow for pulling the distribution from the drupal
; git repository;
projects[re_panopoly][download][type] = git
projects[re_panopoly][download][url] = git://bitbucket.org/reyebrow/re_panopoly.git
projects[re_panopoly][download][branch] = master
; You can optionally build from a local directory using the make_local drush
; module found at http://drupal.org/project/make_local.
;projects[re_panopoly][download][type] = local
;projects[re_panopoly][download][source] = /Users/colin/workspace/drupal/re_panopoly
